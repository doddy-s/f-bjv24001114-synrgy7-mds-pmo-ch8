package com.doddysujatmiko.binarfud.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.doddysujatmiko.binarfud.exceptions.NotFoundException;
import com.doddysujatmiko.binarfud.merchant.MerchantRepository;
import com.doddysujatmiko.binarfud.product.dtos.CreateProductDTO;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MerchantRepository merchantRepository;

    @Transactional(rollbackFor = Throwable.class)
    public ProductEntity create(CreateProductDTO req) {
        var product = new ProductEntity();
        product.setName(req.getName());
        product.setPrice(req.getPrice());
        product.setMerchant(merchantRepository.findById(req.getMerchantId()).get());

        return productRepository.save(product);
    }

    @Transactional(readOnly = true)
    public ProductEntity read(Long id) {
        var product = productRepository.findById(id);

        if(product.isEmpty()) {
            throw new NotFoundException("Product doesn't exist");
        }

        return product.get();
    }

    @Transactional(rollbackFor = Throwable.class)
    public void delete(Long id) {
        var product = productRepository.findById(id);

        if(product.isEmpty()) {
            throw new NotFoundException("Product doesn't exist");
        }

        productRepository.delete(product.get());
    }

    @Transactional(rollbackFor = Throwable.class)
    public ProductEntity update(Long id, CreateProductDTO req) {
        var product = productRepository.findById(id);

        if(product.isEmpty()) {
            throw new NotFoundException("Product doesn't exist");
        }

        product.get().setName(req.getName());
        product.get().setPrice(req.getPrice());
        product.get().setMerchant(merchantRepository.findById(req.getMerchantId()).get());

        return productRepository.save(product.get());
    }
}
