package com.doddysujatmiko.binarfud.product;

import com.doddysujatmiko.binarfud.merchant.MerchantEntity;
import com.doddysujatmiko.binarfud.order.OrderDetailsEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "products")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @JsonIgnore
    @ManyToOne(targetEntity = MerchantEntity.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name="merchant_id", referencedColumnName = "id")
    private MerchantEntity merchant;

    @JsonIgnore
    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
    private List<OrderDetailsEntity> orderDetails;
}
