package com.doddysujatmiko.binarfud.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.doddysujatmiko.binarfud.product.dtos.CreateProductDTO;
import com.doddysujatmiko.binarfud.utils.Responser;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity<?> postProduct(@RequestBody CreateProductDTO req) {
        return Responser.constructSuccess(productService.create(req), "Product created", HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<?> getProduct(@RequestParam(name = "productId", required = true) Long productId) {
        return Responser.constructSuccess(productService.read(productId), "Get product", HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteProduct(@RequestParam(name = "productId", required = true) Long productId) {
        productService.delete(productId);
        return Responser.constructSuccess("", "Product deleted", HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> putProduct(@RequestParam(name = "productId", required = true) Long productId,
                                       @RequestBody CreateProductDTO req) {
        return Responser.constructSuccess(productService.update(productId, req), "Product updated", HttpStatus.OK);
    }

}
