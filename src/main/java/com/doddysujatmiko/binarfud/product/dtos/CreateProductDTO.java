package com.doddysujatmiko.binarfud.product.dtos;

import lombok.Data;

@Data
public class CreateProductDTO {
    private String name;
    private Integer price;
    private Long merchantId;
}
