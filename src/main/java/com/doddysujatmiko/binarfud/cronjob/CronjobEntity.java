package com.doddysujatmiko.binarfud.cronjob;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "cronjobs")
public class CronjobEntity {
    @Id
    Long id;

    String name;

    String schedule;
}
