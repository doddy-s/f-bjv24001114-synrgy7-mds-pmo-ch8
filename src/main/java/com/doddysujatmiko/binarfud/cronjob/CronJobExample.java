package com.doddysujatmiko.binarfud.cronjob;

import com.doddysujatmiko.binarfud.email.dtos.EmailDTO;
import com.doddysujatmiko.binarfud.firebase.FirebaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Component
public class CronJobExample {
    @Autowired
    FirebaseService firebaseService;

    @Autowired
    @Qualifier(value = "taskExecutor")
    private TaskExecutor taskExecutor;

    @Scheduled(cron = "${cron.expression:-}")
    public void sendAsync() {
        taskExecutor.execute(() -> {
            System.out.println("Current time is :: " + new Date());
            firebaseService.sendPushNotificationService();
        });
    }
}
