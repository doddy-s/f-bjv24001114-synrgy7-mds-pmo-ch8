package com.doddysujatmiko.binarfud.cronjob;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CronjobRepository extends JpaRepository<CronjobEntity, Long> {
}
