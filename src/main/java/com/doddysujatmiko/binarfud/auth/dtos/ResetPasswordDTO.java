package com.doddysujatmiko.binarfud.auth.dtos;

import lombok.Data;

@Data
public class ResetPasswordDTO {
    private String username;
    private String email;
    private String password;
}
