package com.doddysujatmiko.binarfud.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Collection;
import java.util.List;

public interface RoleRepository extends JpaRepository<RoleEntity, String> {
    List<RoleEntity> findByNameIn(Collection<String> name);
    RoleEntity findByName(String name);
    Boolean existsByName(String name);
}
