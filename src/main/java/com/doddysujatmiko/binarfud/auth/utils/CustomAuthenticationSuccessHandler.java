package com.doddysujatmiko.binarfud.auth.utils;

import com.doddysujatmiko.binarfud.auth.UserRepository;
import com.doddysujatmiko.binarfud.utils.Generator;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private UserRepository userRepository;

    @Value("server.servlet.context-path")
    private String contextPath;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        var user = userRepository.findByUsername(authentication.getName());
        user.ifPresent(u -> u.setOtp(Generator.generateOTP(16)));
        userRepository.save(user.get());

        response.sendRedirect(contextPath + "/auth/google/success?username=" + user.get().getUsername() + "&otp=" + user.get().getOtp());
    }
}
