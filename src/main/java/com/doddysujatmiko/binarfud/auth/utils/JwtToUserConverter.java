package com.doddysujatmiko.binarfud.auth.utils;

import com.doddysujatmiko.binarfud.auth.UserEntity;
import com.doddysujatmiko.binarfud.auth.UserRepository;
import com.doddysujatmiko.binarfud.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class JwtToUserConverter implements Converter<Jwt, UsernamePasswordAuthenticationToken> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UsernamePasswordAuthenticationToken convert(Jwt source) {
        Optional<UserEntity> user = userRepository.findById(source.getSubject());

        if(user.isEmpty()) {
            throw new NotFoundException("User not registered");
        }

        return new UsernamePasswordAuthenticationToken(user.get(), source, user.get().getAuthorities());
    }
}