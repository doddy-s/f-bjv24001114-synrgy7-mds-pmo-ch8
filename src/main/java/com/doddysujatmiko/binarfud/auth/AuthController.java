package com.doddysujatmiko.binarfud.auth;

//public class AuthController {}


import com.doddysujatmiko.binarfud.auth.dtos.LoginDTO;
import com.doddysujatmiko.binarfud.auth.dtos.RegisterDTO;
import com.doddysujatmiko.binarfud.auth.dtos.ResetPasswordDTO;
import com.doddysujatmiko.binarfud.auth.utils.TokenGenerator;
import com.doddysujatmiko.binarfud.utils.Responser;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
//@ConditionalOnExpression("false")
public class AuthController {
    @Autowired
    AuthService authService;

    @Autowired
    TokenGenerator tokenGenerator;

    @Autowired
    DaoAuthenticationProvider daoAuthenticationProvider;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegisterDTO req) {
        UserEntity user = new UserEntity();
        user.setUsername(req.getUsername());
        user.setEmail(req.getEmail());
        user.setPassword(req.getPassword());
        authService.createUser(user);

        Authentication authentication = daoAuthenticationProvider.authenticate(UsernamePasswordAuthenticationToken.unauthenticated(req.getUsername(), req.getPassword()));

        return Responser.constructSuccess(tokenGenerator.createToken(authentication), "User successfully created", HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDTO req) {
        String username = req.getUsername();
        if(username.isEmpty()) {
            username = authService.loadUserByEmail(req.getEmail()).getUsername();
        }

        Authentication authentication = daoAuthenticationProvider.authenticate(UsernamePasswordAuthenticationToken.unauthenticated(username, req.getPassword()));

        return Responser.constructSuccess(tokenGenerator.createToken(authentication), "Logged in", HttpStatus.OK);
    }

    @GetMapping("/google/success")
    public ResponseEntity<?> handleSuccessedGoogleRedirect(@RequestParam("username") String username,
                                                           @RequestParam("otp") String otp) {
        var user = authService.loadUserWithOTP(username, otp);
        Authentication authentication = UsernamePasswordAuthenticationToken.authenticated(user, user.getPassword(), user.getAuthorities());
        return Responser.constructSuccess(tokenGenerator.createToken(authentication), "Logged in with google.", HttpStatus.OK);
    }

    @Hidden
    @GetMapping("/google/failure")
    public ResponseEntity<?> handleFailedGoogleRedirect() {
        return Responser.constructError("Google login failed", HttpStatus.OK);
    }

    @GetMapping("/password/forget")
    public ResponseEntity<?> forgetPassword(@RequestParam("email") String email) {
        authService.sendRecoveryEmail(email);
        return Responser.constructSuccess("", "OTP sent to your email", HttpStatus.OK);
    }

    @PostMapping("/password/reset")
    public ResponseEntity<?> resetPassword(@RequestParam("otp") String otp,
                                           @RequestBody ResetPasswordDTO req) {
        authService.resetPassword(otp, req);
        return Responser.constructSuccess("", "Password reset", HttpStatus.OK);
    }
}
