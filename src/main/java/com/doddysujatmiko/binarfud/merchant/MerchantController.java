package com.doddysujatmiko.binarfud.merchant;

import com.doddysujatmiko.binarfud.merchant.dtos.CreateMerchantDTO;
import com.doddysujatmiko.binarfud.utils.Responser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/merchant")
public class MerchantController {
    @Autowired
    private MerchantService merchantService;

    @GetMapping
    public ResponseEntity<?> getOrder(@RequestParam("id") Long id) {
        return Responser.constructSuccess(merchantService.read(id), "Merchant found", HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> postMerchant(@RequestBody CreateMerchantDTO req, Principal principal) {
        return Responser.constructSuccess(merchantService.create(req, principal), "Merchant created", HttpStatus.CREATED);
    }

    @GetMapping("/open")
    public ResponseEntity<?> handleSuccessedGoogleRedirect(@RequestParam("id") Long id,
                                                           @RequestParam("open") boolean open) {
        return Responser.constructSuccess(merchantService.editOpenStatus(id, open), "Merchant open status set", HttpStatus.OK);
    }
}
