package com.doddysujatmiko.binarfud.merchant.dtos;

import lombok.Data;

@Data
public class CreateMerchantDTO {
    private String name;
    private String location;
}
