package com.doddysujatmiko.binarfud.merchant;

import com.doddysujatmiko.binarfud.auth.UserEntity;
import com.doddysujatmiko.binarfud.product.ProductEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "merchants")
public class MerchantEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String name;

    private String location;

    private Boolean open;

    @JsonIgnore
    @OneToMany(mappedBy = "merchant", cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private List<ProductEntity> products;

    @JsonIgnore
    @ManyToOne(targetEntity = UserEntity.class, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name="createdBy", referencedColumnName = "id")
    private UserEntity createdBy;
}
