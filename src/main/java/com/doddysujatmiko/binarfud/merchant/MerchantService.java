package com.doddysujatmiko.binarfud.merchant;

import com.doddysujatmiko.binarfud.auth.UserRepository;
import com.doddysujatmiko.binarfud.exceptions.NotFoundException;
import com.doddysujatmiko.binarfud.merchant.dtos.CreateMerchantDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.NotActiveException;
import java.security.Principal;

@Service
public class MerchantService {
    @Autowired
    MerchantRepository merchantRepository;

    @Autowired
    UserRepository userRepository;

    @Transactional(rollbackFor = Throwable.class)
    public MerchantEntity create(CreateMerchantDTO req, Principal principal) {
        var merchant = new MerchantEntity();
        merchant.setName(req.getName());
        merchant.setLocation(req.getLocation());
        merchant.setOpen(true);
        merchant.setCreatedBy(userRepository.findByUsername(principal.getName()).get());

        return merchantRepository.save(merchant);
    }

    public MerchantEntity editOpenStatus(Long id, Boolean open) {
        var merchant = merchantRepository.findById(id);

        if(merchant.isEmpty()) {
            throw new NotFoundException("Merchant not found");
        }

        merchant.ifPresent(m -> m.setOpen(open));

        return merchantRepository.save(merchant.get());
    }

    @Transactional(readOnly = true)
    public MerchantEntity read(Long id) {
        var merchant = merchantRepository.findById(id);

        if(merchant.isEmpty()) {
            throw new NotFoundException("Merchant not found");
        }

        return merchant.get();
    }
}
