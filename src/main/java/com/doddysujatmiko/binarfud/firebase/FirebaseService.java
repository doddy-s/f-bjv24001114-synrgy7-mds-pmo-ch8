package com.doddysujatmiko.binarfud.firebase;

import com.doddysujatmiko.binarfud.exceptions.BadRequestException;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import org.springframework.stereotype.Service;

@Service
public class FirebaseService {
    public Boolean sendPushNotificationService() {
        try {
            Message message = Message
                    .builder()
                    .setTopic("topic")
                    .putData("title", "Promo di BinarFud!")
                    .putData("body", "Promo diskon 20% untuk semua menu di BinarFud! Buruan pesan sekarang!")
                    .build();

            FirebaseMessaging.getInstance().send(message);

            return true;
        } catch (Throwable t) {
            t.printStackTrace();
            return false;
        }
    }
}
