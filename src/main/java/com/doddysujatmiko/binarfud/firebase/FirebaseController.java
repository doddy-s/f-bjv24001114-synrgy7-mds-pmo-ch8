package com.doddysujatmiko.binarfud.firebase;

import com.doddysujatmiko.binarfud.utils.Responser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/firebase")
public class FirebaseController {
    @Autowired
    private FirebaseService firebaseService;

    @GetMapping
    public ResponseEntity<?> sendPushNotification() {
        return Responser.constructSuccess(firebaseService.sendPushNotificationService(), "Push notification sent", HttpStatus.OK);
    }
}
