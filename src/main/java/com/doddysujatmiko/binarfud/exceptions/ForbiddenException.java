package com.doddysujatmiko.binarfud.exceptions;

public class ForbiddenException extends RuntimeException{
    public ForbiddenException(String s) {
        super(s);
    }
}
