package com.doddysujatmiko.binarfud.order;

import com.doddysujatmiko.binarfud.auth.UserRepository;
import com.doddysujatmiko.binarfud.exceptions.BadRequestException;
import com.doddysujatmiko.binarfud.exceptions.ForbiddenException;
import com.doddysujatmiko.binarfud.exceptions.NotFoundException;
import com.doddysujatmiko.binarfud.order.dtos.CreateOrderDTO;
import com.doddysujatmiko.binarfud.order.dtos.CreateOrderDetailsDTO;
import com.doddysujatmiko.binarfud.product.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailsRepository orderDetailsRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Transactional(rollbackFor = Throwable.class)
    public OrderEntity create(CreateOrderDTO req, Principal principal) {
        var order = new OrderEntity();
        order.setDestinationAddress(req.getDestinationAddress());
        order.setIsCompleted(req.getIsCompleted());
        order.setUser(userRepository.findByUsername(principal.getName()).get());

        return orderRepository.save(order);
    }

    @Transactional(readOnly = true)
    public List<OrderEntity> read(Principal principal) {
        return orderRepository.findAllByUser(userRepository.findByUsername(principal.getName()).get());
    }

    @Transactional(rollbackFor = Throwable.class)
    public void delete(Long id, Principal principal) {
        var order = orderRepository.findById(id);

        if(order.isEmpty()) {
            throw new NotFoundException("Order doesn't exist");
        }

        if(!order.get().getUser().getUsername().equals(principal.getName())) {
            throw new ForbiddenException("This order is not yours");
        }

        orderRepository.delete(order.get());
    }

    @Transactional(rollbackFor = Throwable.class)
    public OrderEntity addOrderDetails(Long id, CreateOrderDetailsDTO req, Principal principal) {
        var order = orderRepository.findById(id);

        if(order.isEmpty()) {
            throw new NotFoundException("Order doesn't exist");
        }

        if(!order.get().getUser().getUsername().equals(principal.getName())) {
            throw new ForbiddenException("This order is not yours");
        }
        
        var orderDetails = new OrderDetailsEntity();

        order.get().getOrderDetails().forEach(orderDetailsEntity -> {
            if(orderDetailsEntity.getProduct().getId().equals(req.getProductId())) {
                throw new BadRequestException("Product already exists in order");
            }
        });

        orderDetails.setProduct(productRepository.findById(req.getProductId()).get());
        orderDetails.setQuantity(req.getQuantity());
        orderDetails.setOrder(order.get());

        return orderDetailsRepository.save(orderDetails).getOrder();
    }
}
