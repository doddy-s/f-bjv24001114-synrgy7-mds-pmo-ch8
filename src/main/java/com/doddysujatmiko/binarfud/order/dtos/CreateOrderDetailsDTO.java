package com.doddysujatmiko.binarfud.order.dtos;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateOrderDetailsDTO {
    @NotNull
    private Long productId;

    private Integer quantity;
}
