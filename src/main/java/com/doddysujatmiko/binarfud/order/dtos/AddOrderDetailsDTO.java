package com.doddysujatmiko.binarfud.order.dtos;

import lombok.Data;

@Data
public class AddOrderDetailsDTO {
    private Long OrderId;
    private CreateOrderDetailsDTO orderDetails;
}
