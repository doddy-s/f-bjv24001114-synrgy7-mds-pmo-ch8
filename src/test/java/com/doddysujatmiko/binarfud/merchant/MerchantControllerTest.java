package com.doddysujatmiko.binarfud.merchant;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
public class MerchantControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MerchantRepository merchantRepository;

    @Test
    public void createOne() throws Exception {
        var expectedData = new MerchantEntity();
        expectedData.setId(1L);
        expectedData.setName("Toko Percobaan");
        expectedData.setLocation("Bandung");
        expectedData.setOpen(true);

        Mockito.when(merchantRepository.findById(1L)).thenReturn(Optional.of(expectedData));

        mockMvc.perform(MockMvcRequestBuilders.get("/merchant/").param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("message", "Merchant found"))
                .andExpect(MockMvcResultMatchers.model().attribute("data", expectedData))
                .andDo(MockMvcResultHandlers.print());
    }
}
